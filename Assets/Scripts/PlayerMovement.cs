﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [Range(1, 4)]
    public int playerID;

    public float speed = 3f;
    public GameObject topPrefab;
    public GameObject topObject;
    public bool canControlTop;
    public float maxDist;
    Vector3 topDir;

    void Start()
    {
        topObject = Instantiate(topPrefab);
        topObject.transform.GetComponent<TopMovement>().player = transform.gameObject;
        topObject.transform.GetComponent<TopMovement>().playerID = playerID;
        //topObject.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial;
        topObject.tag = "Top " + playerID;
        topObject.transform.position = transform.position + Vector3.forward * -2f + Vector3.up;
    }

    void Update()
    {
        float movementX = Input.GetAxisRaw("Player Horizontal " + playerID);
        float movementY = Input.GetAxisRaw("Player Vertical " + playerID);
        float xSpd = movementX * speed;
        float ySpd = movementY * speed;
        Vector3 movSpd = xSpd * Vector3.right + ySpd * Vector3.forward;

        //cap speed
        if (movSpd.magnitude > speed)
            movSpd = movSpd.normalized * speed;

        transform.Translate(movSpd * Time.deltaTime);

        topDir = topObject.transform.position - transform.position;
        float topDist = Vector3.Distance(transform.position, topObject.transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, topDir, out hit))
        {
            if (hit.transform.gameObject.tag == "Top " + playerID  && topDist <= maxDist)
                canControlTop = true;
            else
                canControlTop = false;
        }

        LineRenderer beam = GetComponent<LineRenderer>();
        float beamLength = Vector3.Distance(transform.position, topObject.transform.position);
        beam.material.SetTextureScale("_MainTex",new Vector2(beamLength / 6, 1));
        beam.SetPosition(0,transform.position);
        beam.SetPosition(1, topObject.transform.position);
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, maxDist);
        
        if (canControlTop){
            Gizmos.color = Color.green;
        } else {
            Gizmos.color = Color.red;
        }
        
        Gizmos.DrawRay(transform.position, topDir);
    }
}
