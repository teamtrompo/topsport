﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopMovement : MonoBehaviour {

    [Range(1, 4)]
    public int playerID;

    public float speed = 2f;
    public GameObject player;

    void Update()
    {
        if (player.GetComponent<PlayerMovement>().canControlTop)
        {
            float movementX = Input.GetAxisRaw("Top Horizontal " + playerID);
            float movementY = Input.GetAxisRaw("Top Vertical " + playerID);
            float xSpd = movementX * speed;
            float ySpd = movementY * speed;
            Vector3 movSpd = xSpd * Vector3.right + ySpd * Vector3.forward;

            //cap speed
            if (movSpd.magnitude > speed)
                movSpd = movSpd.normalized * speed;

            Rigidbody rb = GetComponent<Rigidbody>();
            rb.AddForce(movSpd * Time.deltaTime, ForceMode.Force);
        }
    }
}
